{
  "swagger" : "2.0",
  "info" : {
    "description" : "DeltaXML JSON Compare REST Service. Go here to find out how to obtain an ID Token - https://www.deltaxml.com/products/json-compare/docs/rest-guide#authentication",
    "version" : "1.0",
    "title" : "JSON Compare",
    "termsOfService" : "https://www.deltaxml.com",
    "contact" : {
      "name" : "DeltaXML",
      "url" : "https://www.deltaxml.com/support-forums/#!/json-compare",
      "email" : "support@deltaxml.com"
    }
  },
  "host" : "www.deltaxml.com",
  "basePath" : "/api/json/v2",
  "schemes" : [ "https" ],
  "paths" : {
    "/compare/json_uris" : {
      "post" : {
        "summary" : "Compare JSON from URI's specified in JSON input",
        "description" : "",
        "operationId" : "compareURIsFromJson",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json", "application/xml" ],
        "parameters" : [ {
          "in" : "body",
          "name" : "comparison",
          "description" : "JSON input",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/Comparison"
          }
        }, {
          "name" : "Accept",
          "in" : "header",
          "description" : "Format returned by REST service (XML or JSON)",
          "required" : true,
          "type" : "string",
          "enum" : [ "application/json", "application/xml" ]
        }, {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "type" : "string"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    },
    "/compare/multi_part_strings" : {
      "post" : {
        "summary" : "Compare raw JSON from Multi-Part Form Data",
        "description" : "",
        "operationId" : "compareStringsFromMultiPart",
        "consumes" : [ "multipart/form-data" ],
        "produces" : [ "application/json", "application/xml" ],
        "parameters" : [ {
          "name" : "Accept",
          "in" : "header",
          "description" : "Format returned by REST service (XML or JSON)",
          "required" : true,
          "type" : "string",
          "enum" : [ "application/json", "application/xml" ]
        }, {
          "name" : "ajson",
          "in" : "formData",
          "description" : "JSON Input A",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "bjson",
          "in" : "formData",
          "description" : "JSON Input B",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "wordbyword",
          "in" : "formData",
          "description" : "Word By Word",
          "required" : false,
          "type" : "string",
          "default" : "false",
          "enum" : [ "true", "false" ]
        }, {
          "name" : "arrayalignment",
          "in" : "formData",
          "description" : "Array Alignment",
          "required" : false,
          "type" : "string",
          "default" : "type-with-value-priority",
          "enum" : [ "type-with-value-priority", "position-priority", "orderless" ]
        }, {
          "name" : "output",
          "in" : "formData",
          "description" : "Output Type",
          "required" : false,
          "type" : "string",
          "default" : "full-context",
          "enum" : [ "full-context", "changes-only" ]
        }, {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "dx_config",
          "in" : "formData",
          "description" : "dx_config JSON object",
          "required" : false,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "type" : "string"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    },
    "/compare/multi_part_uris" : {
      "post" : {
        "summary" : "Compare JSON from URI's specified in Multi-Part Form Data",
        "description" : "",
        "operationId" : "compareURIsFromMultiPart",
        "consumes" : [ "multipart/form-data" ],
        "produces" : [ "application/json", "application/xml" ],
        "parameters" : [ {
          "name" : "Accept",
          "in" : "header",
          "description" : "Format returned by REST service (XML or JSON)",
          "required" : true,
          "type" : "string",
          "enum" : [ "application/json", "application/xml" ]
        }, {
          "name" : "ajson",
          "in" : "formData",
          "description" : "URI for Input A",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "bjson",
          "in" : "formData",
          "description" : "URI for Input B",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "wordbyword",
          "in" : "formData",
          "description" : "Word By Word",
          "required" : false,
          "type" : "string",
          "default" : "false",
          "enum" : [ "true", "false" ]
        }, {
          "name" : "arrayalignment",
          "in" : "formData",
          "description" : "Array Alignment",
          "required" : false,
          "type" : "string",
          "default" : "type-with-value-priority",
          "enum" : [ "type-with-value-priority", "position-priority", "orderless" ]
        }, {
          "name" : "output",
          "in" : "formData",
          "description" : "Output Type",
          "required" : false,
          "type" : "string",
          "default" : "full-context",
          "enum" : [ "full-context", "changes-only" ]
        }, {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "dx_config",
          "in" : "formData",
          "description" : "dx_config JSON object",
          "required" : false,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "type" : "string"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    },
    "/graft/json_uris" : {
      "post" : {
        "summary" : "Graft JSON from URI's specified in JSON input",
        "description" : "",
        "operationId" : "graftURIsFromJson",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json", "application/xml" ],
        "parameters" : [ {
          "in" : "body",
          "name" : "graft",
          "description" : "JSON input",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/Graft"
          }
        }, {
          "name" : "Accept",
          "in" : "header",
          "description" : "Format returned by REST service (XML or JSON)",
          "required" : true,
          "type" : "string",
          "enum" : [ "application/json", "application/xml" ]
        }, {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "type" : "string"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    },
    "/graft/multi_part_strings" : {
      "post" : {
        "summary" : "Graft JSON from Multi-Part Form Data",
        "description" : "",
        "operationId" : "graftStringsFromMultiPart",
        "consumes" : [ "multipart/form-data" ],
        "produces" : [ "application/json", "application/xml" ],
        "parameters" : [ {
          "name" : "Accept",
          "in" : "header",
          "description" : "Format returned by REST service (XML or JSON)",
          "required" : true,
          "type" : "string",
          "enum" : [ "application/json", "application/xml" ]
        }, {
          "name" : "changeset",
          "in" : "formData",
          "description" : "JSON Graft Changeset",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "target",
          "in" : "formData",
          "description" : "JSON Graft Target",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "outputmode",
          "in" : "formData",
          "description" : "Output Mode",
          "required" : false,
          "type" : "string",
          "default" : "changeset-priority",
          "enum" : [ "changeset-priority", "target-priority", "additions-only" ]
        }, {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "type" : "string"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    },
    "/graft/multi_part_uris" : {
      "post" : {
        "summary" : "Merge JSON from URIs specified in Multi-Part Form Data",
        "description" : "",
        "operationId" : "graftURIsFromMultiPart",
        "consumes" : [ "multipart/form-data" ],
        "produces" : [ "application/json", "application/xml" ],
        "parameters" : [ {
          "name" : "Accept",
          "in" : "header",
          "description" : "Format returned by REST service (XML or JSON)",
          "required" : true,
          "type" : "string",
          "enum" : [ "application/json", "application/xml" ]
        }, {
          "name" : "changeset",
          "in" : "formData",
          "description" : "JSON Graft Changeset",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "target",
          "in" : "formData",
          "description" : "JSON Graft Target",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "outputmode",
          "in" : "formData",
          "description" : "Output Mode",
          "required" : false,
          "type" : "string",
          "default" : "changeset-priority",
          "enum" : [ "changeset-priority", "target-priority", "additions-only" ]
        }, {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "type" : "string"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    },
    "/merge/json_uris" : {
      "post" : {
        "summary" : "Two-Way Merge JSON from URI's specified in JSON input",
        "description" : "",
        "operationId" : "mergeURIsfromJSON",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json", "application/xml" ],
        "parameters" : [ {
          "in" : "body",
          "name" : "merge",
          "description" : "JSON input",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/Merge"
          }
        }, {
          "name" : "Accept",
          "in" : "header",
          "description" : "Format returned by REST service (XML or JSON)",
          "required" : true,
          "type" : "string",
          "enum" : [ "application/json", "application/xml" ]
        }, {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "type" : "string"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    },
    "/merge/multi_part_strings" : {
      "post" : {
        "summary" : "Two-Way Merge JSON from Multi-Part Form Data",
        "description" : "",
        "operationId" : "mergeStringsFromMultiPart",
        "consumes" : [ "multipart/form-data" ],
        "produces" : [ "application/json", "application/xml" ],
        "parameters" : [ {
          "name" : "Accept",
          "in" : "header",
          "description" : "Format returned by REST service (XML or JSON)",
          "required" : true,
          "type" : "string",
          "enum" : [ "application/json", "application/xml" ]
        }, {
          "name" : "ajson",
          "in" : "formData",
          "description" : "JSON Input A",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "bjson",
          "in" : "formData",
          "description" : "JSON Input B",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "wordbyword",
          "in" : "formData",
          "description" : "Word By Word",
          "required" : false,
          "type" : "string",
          "default" : "false",
          "enum" : [ "true", "false" ]
        }, {
          "name" : "arrayalignment",
          "in" : "formData",
          "description" : "Array Alignment",
          "required" : false,
          "type" : "string",
          "default" : "type-with-value-priority",
          "enum" : [ "type-with-value-priority", "position-priority", "orderless" ]
        }, {
          "name" : "mergepriority",
          "in" : "formData",
          "description" : "Merge Priority",
          "required" : false,
          "type" : "string",
          "default" : "a",
          "enum" : [ "a", "b" ]
        }, {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "dx_config",
          "in" : "formData",
          "description" : "dx_config JSON object",
          "required" : false,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "type" : "string"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    },
    "/merge/multi_part_uris" : {
      "post" : {
        "summary" : "Two-Way Merge JSON from URIs specified in Multi-Part Form Data",
        "description" : "",
        "operationId" : "mergeURIsfromMultiPart",
        "consumes" : [ "multipart/form-data" ],
        "produces" : [ "application/json", "application/xml" ],
        "parameters" : [ {
          "name" : "Accept",
          "in" : "header",
          "description" : "Format returned by REST service (XML or JSON)",
          "required" : true,
          "type" : "string",
          "enum" : [ "application/json", "application/xml" ]
        }, {
          "name" : "ajson",
          "in" : "formData",
          "description" : "URI for Input A",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "bjson",
          "in" : "formData",
          "description" : "URI for Input B",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "wordbyword",
          "in" : "formData",
          "description" : "Word By Word",
          "required" : false,
          "type" : "string",
          "default" : "false",
          "enum" : [ "true", "false" ]
        }, {
          "name" : "arrayalignment",
          "in" : "formData",
          "description" : "Array Alignment",
          "required" : false,
          "type" : "string",
          "default" : "type-with-value-priority",
          "enum" : [ "type-with-value-priority", "position-priority", "orderless" ]
        }, {
          "name" : "mergepriority",
          "in" : "formData",
          "description" : "Merge Priority",
          "required" : false,
          "type" : "string",
          "default" : "a",
          "enum" : [ "a", "b" ]
        }, {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "dx_config",
          "in" : "formData",
          "description" : "dx_config JSON object",
          "required" : false,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "type" : "string"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    },
    "/patch/json_uris" : {
      "post" : {
        "summary" : "Generate JSON Patch from URIs specified in JSON",
        "description" : "",
        "operationId" : "patchURIsFromJson",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json", "application/xml" ],
        "parameters" : [ {
          "in" : "body",
          "name" : "patch",
          "description" : "JSON input",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/Patch"
          }
        }, {
          "name" : "Accept",
          "in" : "header",
          "description" : "Format returned by REST service (XML or JSON)",
          "required" : true,
          "type" : "string",
          "enum" : [ "application/json", "application/xml" ]
        }, {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "type" : "string"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    },
    "/patch/multi_part_strings" : {
      "post" : {
        "summary" : "Generate JSON Patch from Multi-Part Form Data",
        "description" : "",
        "operationId" : "patchStringsFromMultiPart",
        "consumes" : [ "multipart/form-data" ],
        "produces" : [ "application/json", "application/xml" ],
        "parameters" : [ {
          "name" : "Accept",
          "in" : "header",
          "description" : "Format returned by REST service (XML or JSON)",
          "required" : true,
          "type" : "string",
          "enum" : [ "application/json", "application/xml" ]
        }, {
          "name" : "ajson",
          "in" : "formData",
          "description" : "JSON Input A",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "bjson",
          "in" : "formData",
          "description" : "JSON Input B",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "patchdirection",
          "in" : "formData",
          "description" : "Patch Direction",
          "required" : false,
          "type" : "string",
          "default" : "a-to-b",
          "enum" : [ "a-to-b", "b-to-a" ]
        }, {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "dx_config",
          "in" : "formData",
          "description" : "dx_config JSON object",
          "required" : false,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "type" : "string"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    },
    "/patch/multi_part_uris" : {
      "post" : {
        "summary" : "Generate JSON Patch from URIs specified in Multi-Part Form Data",
        "description" : "",
        "operationId" : "patchURIsFromMultiPart",
        "consumes" : [ "multipart/form-data" ],
        "produces" : [ "application/json", "application/xml" ],
        "parameters" : [ {
          "name" : "Accept",
          "in" : "header",
          "description" : "Format returned by REST service (XML or JSON)",
          "required" : true,
          "type" : "string",
          "enum" : [ "application/json", "application/xml" ]
        }, {
          "name" : "ajson",
          "in" : "formData",
          "description" : "URI for Input A",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "bjson",
          "in" : "formData",
          "description" : "URI for Input B",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "patchdirection",
          "in" : "formData",
          "description" : "Patch Direction",
          "required" : false,
          "type" : "string",
          "default" : "a-to-b",
          "enum" : [ "a-to-b", "b-to-a" ]
        }, {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "dx_config",
          "in" : "formData",
          "description" : "dx_config JSON object",
          "required" : false,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "type" : "string"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    },
    "/usage" : {
      "get" : {
        "summary" : "Returns usage information in JSON format",
        "description" : "",
        "operationId" : "getUsageCount",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "Authorization",
          "in" : "header",
          "description" : "auth0 ID Token - i.e. 'Bearer ID_TOKEN'",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "$ref" : "#/definitions/usage"
            }
          },
          "400" : {
            "description" : "HTTP 400 Bad Request"
          },
          "500" : {
            "description" : "Internal Server Error"
          }
        }
      }
    }
  },
  "definitions" : {
    "Comparison" : {
      "type" : "object",
      "required" : [ "ajson", "bjson" ],
      "properties" : {
        "ajson" : {
          "type" : "string",
          "format" : "uri",
          "description" : "URI to JSON Input A"
        },
        "bjson" : {
          "type" : "string",
          "format" : "uri",
          "description" : "URI to JSON Input B"
        },
        "wordbyword" : {
          "type" : "boolean",
          "description" : "Option to process using WordIn and WordOut filters",
          "enum" : [ true, false ]
        },
        "arrayalignment" : {
          "type" : "string",
          "description" : "Option for how to process Arrays",
          "enum" : [ "type-with-value-priority", "position-priority", "orderless" ]
        },
        "output" : {
          "type" : "string",
          "description" : "Option to return full context Delta or changes only",
          "enum" : [ "full-context", "changes-only" ]
        },
        "dx_config" : {
          "type" : "array",
          "items" : {
            "$ref" : "#/definitions/dx_config"
          }
        }
      },
      "description" : "This class handles Comparisons with JSON input"
    },
    "Graft" : {
      "type" : "object",
      "required" : [ "changeset", "target" ],
      "properties" : {
        "changeset" : {
          "type" : "string",
          "format" : "uri",
          "description" : "URI to JSON Graft Changeset"
        },
        "target" : {
          "type" : "string",
          "format" : "uri",
          "description" : "URI to JSON Graft Target"
        },
        "outputmode" : {
          "type" : "string",
          "description" : "Output Mode of the Graft",
          "enum" : [ "changeset-priority", "target-priority", "additions-only" ]
        }
      },
      "description" : "This class handles JSON input for Grafting"
    },
    "Merge" : {
      "type" : "object",
      "required" : [ "ajson", "bjson" ],
      "properties" : {
        "ajson" : {
          "type" : "string",
          "format" : "uri",
          "description" : "URI to JSON Input A"
        },
        "bjson" : {
          "type" : "string",
          "format" : "uri",
          "description" : "URI to JSON Input B"
        },
        "wordbyword" : {
          "type" : "boolean",
          "description" : "Option to process using WordIn and WordOut filters",
          "enum" : [ true, false ]
        },
        "arrayalignment" : {
          "type" : "string",
          "description" : "Option for how to process Arrays",
          "enum" : [ "type-with-value-priority", "position-priority", "orderless" ]
        },
        "mergepriority" : {
          "type" : "string",
          "description" : "Option for which input to give priority to when performing the Merge",
          "enum" : [ "a", "b" ]
        },
        "dx_config" : {
          "type" : "array",
          "description" : "Array of objects containing element level configuration (e.g. ignorechanges)",
          "items" : {
            "$ref" : "#/definitions/dx_config"
          }
        }
      },
      "description" : "This class handles JSON input for Merges"
    },
    "Patch" : {
      "type" : "object",
      "required" : [ "ajson", "bjson" ],
      "properties" : {
        "ajson" : {
          "type" : "string",
          "format" : "uri",
          "description" : "URI to JSON Input A"
        },
        "bjson" : {
          "type" : "string",
          "format" : "uri",
          "description" : "URI to JSON Input B"
        },
        "patchdirection" : {
          "type" : "string",
          "description" : "Option for direction of Patch",
          "enum" : [ "a-to-b", "b-to-a" ]
        },
        "dx_config" : {
          "type" : "array",
          "description" : "Array of objects containing element level configuration (e.g. ignorechanges)",
          "items" : {
            "$ref" : "#/definitions/dx_config"
          }
        }
      },
      "description" : "This class handles JSON input for Patches"
    },
    "dx_config" : {
      "type" : "object",
      "properties" : {
        "ignorechanges" : {
          "type" : "string",
          "description" : "Type of ignorechanges",
          "enum" : [ "delete" ]
        },
        "key" : {
          "type" : "string",
          "description" : "Key to ignore"
        }
      },
      "description" : "This class handles dx_config objects for operations"
    },
    "usage" : {
      "type" : "object",
      "required" : [ "endPeriod", "permittedUsage", "startPeriod", "usage" ],
      "properties" : {
        "usage" : {
          "type" : "integer",
          "format" : "int32",
          "description" : "current usage level"
        },
        "permittedUsage" : {
          "type" : "integer",
          "format" : "int32",
          "description" : "allowed usage level"
        },
        "startPeriod" : {
          "type" : "string",
          "description" : "the start of the usage reporting period"
        },
        "endPeriod" : {
          "type" : "string",
          "description" : "the end of the current usage reporting period"
        }
      },
      "description" : "This class describes service usage levels"
    }
  }
}