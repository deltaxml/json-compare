![JSON Compare Text Outline.png](https://bitbucket.org/repo/4ppMjKg/images/1241282564-JSON%20Compare%20Text%20Outline.png)

JSON Compare is currently available as REST SaaS offering.  Future releases may provide ways of running the service on your own hardware.

### Repository contents ###

* Swagger definiton
* Pointers to samples
* Documentation

### How do I get set up? ###

* Login (and register) and then download your JWT Token
* Experiment using Postman 
* Look at the samples
* Swagger generated documentation
* Use the API

### Service usage and limitations ###

* Free usage tier - 1000 comparisons per 30 days
* Usage resets after a month

### Support ###

* If the issue is related to a sample please either create an issue in the issue tracker or fork and generate a pull request
* Use the [Support Forum](https://www.deltaxml.com/support-forums/#!/json-compare) on our website